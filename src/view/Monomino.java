package view;

import java.awt.Color;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**Classe utilizzata per visualizzare a schermo 
 * un singolo monomino (blocco)*/
public class Monomino extends JLabel{

	/**Enumeratore che definisce le direzioni di spostamento*/
	public static enum Direzione {destra, sinistra, giu, su};

	public static final int SIZE = 25;
	
	/**Genera un nuovo monomino (oggetto grafico) del colore specificato
	 * @param x			La posizione sull'asse x del contenitore dove posizionare il monomino
	 * @param y			La posizione sull'asse y del contenitore dove posizionare il monomino
	 * @param colore 	Il colore del monomino da creare*/
	public Monomino(int x, int y, Color colore) {
		super();
		if(colore.equals(Color.YELLOW))
			setIcon(new ImageIcon("img/monominogiallo.png"));
		else if(colore.equals(Color.RED))
			setIcon(new ImageIcon("img/monominorosso.png"));
		else if(colore.equals(Color.GREEN))
			setIcon(new ImageIcon("img/monominoverde.png"));
		else if(colore.equals(Color.ORANGE))
			setIcon(new ImageIcon("img/monominoarancione.png"));
		else if(colore.equals(Color.BLUE))
			setIcon(new ImageIcon("img/monominoblu.png"));
		else if(colore.equals(Color.CYAN))
			setIcon(new ImageIcon("img/monominoazzurro.png"));
		else if(colore.equals(Color.MAGENTA))
			setIcon(new ImageIcon("img/monominoviola.png"));
		setBounds(new Rectangle(x, y, SIZE, SIZE));
	}

	public int getRiga() {
		return getLocation().y/SIZE;
	}
	
	public int getColonna() {
		return getLocation().x/SIZE;
	}

	public void setPosizione(int riga, int colonna){
		setLocation(colonna*SIZE, riga*SIZE);
	}
	
	public static Direzione reverse(Direzione d) {
		switch (d) {
		case su:
			return Direzione.giu;
		case giu:
			return Direzione.su;
		case destra:
			return Direzione.sinistra;
		case sinistra:
			return Direzione.destra;
		}
		return d;
	}
	
	/**
	 * Muove il monomino di Monomino.SIZE pixel nella direzione indicata dal parametro d.
	 * */
	public void muovi(Direzione d){
		switch(d){
		case giu:
			setPosizione(getRiga() + 1, getColonna());
			break;
		case destra:
			setPosizione(getRiga(), getColonna() + 1);
			break;
		case sinistra:
			setPosizione(getRiga(), getColonna() - 1);
			break;
		case su:
			setPosizione(getRiga() - 1, getColonna());
			break;
		}
	}
	
	/**
	 * Muove il monomino di Monomino.SIZE * blocks pixel nella direzione indicata dal parametro d.
	 * */
	public void muovi(Direzione d, int blocks){
		if(blocks < 0) {
			d = reverse(d);
			blocks *= -1;
		}
		switch(d){
		case giu:
			setPosizione(getRiga() + blocks, getColonna());
			break;
		case destra:
			setPosizione(getRiga(), getColonna() + blocks);
			break;
		case sinistra:
			setPosizione(getRiga(), getColonna() - blocks);
			break;
		case su:
			setPosizione(getRiga() - blocks, getColonna());
			break;
		}
	}
}
