package view;

import javax.swing.JPanel;

import view.Monomino.Direzione;

import model.Tetromino;


public class Griglia extends JPanel {
	
	public interface OnGameEndListener { 
		void onGameEnd(); 
	}
	
	private Monomino[][] griglia;
	private int larghezza;
	private int altezza;
	private OnGameEndListener endGame;
	
	/**Costruttore della griglia grafica di gioco
	 * @param x la posizione sull'asse x del container
	 * @param y la posizione sull'asse y del container
	 * @param larghezza la larghezza della griglia (indicata in monomini)
	 * @param altezza l'altezza della griglia (indicata in monomini)*/
	public Griglia(int x, int y, int altezza, int larghezza, OnGameEndListener endGame) {
		super(null);
		setBounds(x, y, larghezza*Monomino.SIZE, altezza*Monomino.SIZE);
		griglia = new Monomino[altezza][larghezza];
		for (int i = 0; i < altezza; i++) {
			for (int j = 0; j < larghezza; j++) {
				griglia[i][j] = null;
			}
		}
		
		this.larghezza = larghezza;
		this.altezza = altezza;
		this.endGame = endGame;
		
		setOpaque(false);
	}
	
	private void shiftDown(int start, int blocks) {
		for(int j = start-1; j > 0; j--)
			for(int k = 0; k < larghezza; k++) {
				griglia[j+blocks][k] = griglia[j][k];
				if(griglia[j+blocks][k] != null)
					griglia[j+blocks][k].muovi(Direzione.giu, blocks);
			}	
		
		/*for(int j = start-1; j > 0; j--){
			for(int k = 0; k < larghezza; k++){
				if(griglia[j-blocks][k] != null) {
					remove(griglia[j-blocks][k]);
				}
				griglia[j-blocks][k] = griglia[j][k];
				
			}
		}*/
		revalidate();
		repaint();
	}
	
	private void removeLines(int start, int blocks) {
		for(int j = start; j < start + blocks; j++)
			for(int k = 0; k < larghezza; k++) {
				remove(griglia[j][k]);
				griglia[j][k] = null;
			}
	}
	
	/**Checks if at least one grid line is full of tetrominoes
	 * @return true  if the line is full
	 * 		   false if the line has at least an empty monomino space*/
	private boolean isLineFull(int line) {
		for (Monomino m : griglia[line])
			if(m == null)
				return false;
		return true;
	}
	
	public void addTetromino(Tetromino t) {
		for(Monomino toAdd : t.getMonominos()) {
			if(!addMonomino(toAdd)) {
				endGame.onGameEnd();
				return;
			}
		}
		int fullLines, startLine;
		for(int j = 0; j < altezza; j++) {
			startLine = 0;
			fullLines = 0;
			if(isLineFull(j)) {	
				fullLines++;
				startLine = j;
				int end = j+5;
				if(end > altezza)
					end = altezza;
				
				for(int k = j+1; k < end; k++){
					if(isLineFull(k))
						fullLines++;
					else
						break;
				}
				removeLines(startLine, fullLines);
				shiftDown(startLine, fullLines);
			}
		}
	}
	
	/**Aggiunge un monomino alla griglia di gioco*/
	public boolean addMonomino(Monomino m) {
		int riga = m.getRiga();
		int colonna = m.getColonna();
		if(riga < 0)
			return false;
		else
			griglia[riga][colonna] = m;
		return true;
	}
	
	public Monomino[][] getMonomini() {
		return this.griglia;
	}
	
	/**
	 * return 0 if in bounds fully (monomino is insertable, position is fully legal)
	 * return 1 if in bounds but in endgame area (legal position but possible game end)
	 * return 2 if out of bounds or inside another monomino (illegal position)
	 * */
	public int inBounds(Monomino m) {
		int riga = m.getRiga();
		int colonna = m.getColonna();
		if(colonna < larghezza && riga < altezza && colonna >= 0 && riga >= -1) { // In bounds
			if(riga >= 0)														  // In game area
				if(griglia[riga][colonna] == null)								  // Collides with other monominoes?
					return 0;
				else
					return 2;
			else
				return 1;
		}
		return 2;
	}
}