package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PannelloSfondo extends JPanel {

	private Image sfondo;
	
	public PannelloSfondo(ImageIcon sfondo, int x, int y, int altezza, int larghezza) {
		super(null);
		setBounds(x, y, altezza, larghezza);
		this.sfondo = sfondo.getImage();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(sfondo, 0, 0, 1000, 720, 0, 0, 1000, 720, null);
	}
}
