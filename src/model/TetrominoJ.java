package model;

import java.awt.Color;
import java.awt.Point;

import view.Griglia;
import view.Monomino;
import view.Monomino.Direzione;

public class TetrominoJ extends Tetromino {

	public TetrominoJ(Point location, Griglia gameGrid) {
		super(location, gameGrid);
	}

	public void rotate() {
		switch(setRotation()) {
		case a:
			monominos[0].muovi(Direzione.destra, 2);
			monominos[1].muovi(Direzione.destra);
			monominos[1].muovi(Direzione.su);
			monominos[3].muovi(Direzione.giu);
			monominos[3].muovi(Direzione.sinistra);
			setRotation(RotationStatus.b);
			break;
		case b:
			monominos[0].muovi(Direzione.giu, 2);
			monominos[1].muovi(Direzione.giu);
			monominos[1].muovi(Direzione.destra);
			monominos[3].muovi(Direzione.su);
			monominos[3].muovi(Direzione.sinistra);
			setRotation(RotationStatus.c);
			break;
		case c:
			monominos[0].muovi(Direzione.sinistra, 2);
			monominos[1].muovi(Direzione.sinistra);
			monominos[1].muovi(Direzione.giu);
			monominos[3].muovi(Direzione.destra);
			monominos[3].muovi(Direzione.su);
			setRotation(RotationStatus.d);
			break;
		case d:
			monominos[0].muovi(Direzione.su, 2);
			monominos[1].muovi(Direzione.su);
			monominos[1].muovi(Direzione.sinistra);
			monominos[3].muovi(Direzione.giu);
			monominos[3].muovi(Direzione.destra);
			setRotation(RotationStatus.a);
			break;
		default:
			break;
		}
	}

	/**Generates the four monominos in a J shaped figure.*/
	@Override
	protected void generateMonominos() {
		Point loc = getLocation();
		monominos[0] = new Monomino(loc.x-(Monomino.SIZE*2), loc.y-(Monomino.SIZE*2), Color.BLUE);
		monominos[1] = new Monomino(loc.x-(Monomino.SIZE*2), loc.y-Monomino.SIZE, Color.BLUE);
		monominos[2] = new Monomino(loc.x-Monomino.SIZE, loc.y-Monomino.SIZE, Color.BLUE);
		monominos[3] = new Monomino(loc.x, loc.y-Monomino.SIZE, Color.BLUE);
	}
}
