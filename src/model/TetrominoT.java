package model;

import java.awt.Color;
import java.awt.Point;

import view.Griglia;
import view.Monomino;
import view.Monomino.Direzione;

public class TetrominoT extends Tetromino {

	public TetrominoT(Point location, Griglia gameGrid) {
		super(location, gameGrid);
	}

	public void rotate() {
		switch(setRotation()) {
		case a:
			monominos[2].muovi(Direzione.destra);
			monominos[2].muovi(Direzione.giu);
			setRotation(RotationStatus.b);
			break;
		case b:
			monominos[3].muovi(Direzione.sinistra);
			monominos[3].muovi(Direzione.giu);
			setRotation(RotationStatus.c);
			break;
		case c:
			monominos[0].muovi(Direzione.sinistra);
			monominos[0].muovi(Direzione.su);
			setRotation(RotationStatus.d);
			break;
		case d:
			monominos[2].muovi(Direzione.destra);
			monominos[2].muovi(Direzione.su);
			shiftMonominos();
			setRotation(RotationStatus.a);
			break;
		default:
			break;
		}
	}

	/**Shift the monomino sub-array [0, 2, 3] by one place left, resulting in [2, 3, 0].*/
	private void shiftMonominos() {
		Monomino dueM = monominos[2];
		Monomino treM = monominos[3];
		monominos[3] = monominos[0];
		monominos[0] = dueM;
		monominos[2] = treM;
	}

	/**Generates the four monominos in a T shaped figure.*/
	@Override
	protected void generateMonominos() {
		Point loc = getLocation();
		monominos[0] = new Monomino(loc.x, loc.y-Monomino.SIZE, Color.MAGENTA);
		monominos[1] = new Monomino(loc.x-Monomino.SIZE, loc.y-Monomino.SIZE, Color.MAGENTA);
		monominos[2] = new Monomino(loc.x-(Monomino.SIZE*2), loc.y-Monomino.SIZE, Color.MAGENTA);
		monominos[3] = new Monomino(loc.x-Monomino.SIZE, loc.y-(Monomino.SIZE*2), Color.MAGENTA);
	}

}
