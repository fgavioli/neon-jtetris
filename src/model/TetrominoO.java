package model;

import java.awt.Color;
import java.awt.Point;

import view.Griglia;
import view.Monomino;

public class TetrominoO extends Tetromino {

	public TetrominoO(Point location, Griglia gameGrid) {
		super(location, gameGrid);
	}

	/**Empty method, you can't rotate a square by 90 degrees and expect a different result.*/
	public void rotate() {}

	/**Generates the four monominos in an O shaped figure.*/
	@Override
	protected void generateMonominos() {
		Point loc = getLocation();
		monominos[0] = new Monomino(loc.x, loc.y-Monomino.SIZE, Color.YELLOW);
		monominos[1] = new Monomino(loc.x-Monomino.SIZE, loc.y-Monomino.SIZE, Color.YELLOW);
		monominos[2] = new Monomino(loc.x-Monomino.SIZE, loc.y-(Monomino.SIZE*2), Color.YELLOW);
		monominos[3] = new Monomino(loc.x, loc.y-(Monomino.SIZE*2), Color.YELLOW);
	}

}
