package model;

import java.awt.Color;
import java.awt.Point;

import view.Griglia;
import view.Monomino;

public class TetrominoI extends Tetromino {

	public TetrominoI(Point location, Griglia gameGrid) {
		super(location, gameGrid);
	}

	public void rotate() {
		switch (setRotation()) {
		case a:
			for (int i = 0; i < monominos.length; i++)
				monominos[i].setPosizione(getRow()-2+i, getColumn());
			setRotation(RotationStatus.b);
			break;
		case b:
			for (int i = 0; i < monominos.length; i++)
				monominos[i].setPosizione(getRow(), getColumn()-2+i);
			setRotation(RotationStatus.c);
			break;
		case c:
			for (int i = 0; i < monominos.length; i++)
				monominos[i].setPosizione(getRow()-2+i, getColumn()-1);
			setRotation(RotationStatus.d);
			break;
		case d:
			for (int i = 0; i < monominos.length; i++)
				monominos[i].setPosizione(getRow()+1, getColumn()-2+i);
			setRotation(RotationStatus.a);
			break;
		default:
			break;
		}
	}
	
	/**Generates the four monominos in a horizontal line*/
	@Override
	protected void generateMonominos() {
		Point loc = getLocation();
		for(int i = 0; i < monominos.length; i++)
			monominos[i] = new Monomino(loc.x+((-2 + i)*Monomino.SIZE), loc.y-Monomino.SIZE, Color.CYAN);
	}
}
