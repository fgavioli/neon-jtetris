package model;

import java.awt.Point;

import view.Griglia;
import view.Monomino;
import view.Monomino.Direzione;

/**Abstract class containing generic attributes/methods
 * common for interacting with every tetromino.*/
public abstract class Tetromino {
	
	/**This enumerator defines the rotation status of the tetromino.
	 * <br/>
	 * {@code a} =   0°	
	 * <br/>
	 * {@code b} =  90°
	 * <br/>
	 * {@code c} = 180°
	 * <br/>
	 * {@code d} = 270°*/
	public enum RotationStatus {a, b, c, d}
	
	/**The four monominos forming the tetromino are stored in this array*/
	protected Monomino[] monominos;
	
	/**Defines the location of this tetromino, subclasses should consider 
	 * this value as the location to draw the first monomino*/
	private Point location;
	
	/**Current rotation status for this tetromino*/
	private RotationStatus rotStatus;
	
	/**Game grid object.
	 * <br/>
	 * TODO: should create an interface for methods used here and pass it instead.*/
	private Griglia grigliaGioco;
	
	/**Tetromino constructor. 
	 * @param location Tetromino spawn location
	 * @param grigliaGioco grid where the tetromino will be placed in.*/
	public Tetromino(Point location, Griglia grigliaGioco){
		this.location = location;
		this.rotStatus = RotationStatus.a;
		this.grigliaGioco = grigliaGioco;
		this.monominos = new Monomino[4];
		generateMonominos();
	}

	/**Moves the tetromino in the supplied direction by one block.
	 * @param d The direction where the tetromino will be translated to.
	 * @return 
	 * 		<code>false</code> if the tetromino couldn't be moved in the supplied direction
	 * 		<br/>
	 * 		<code>true</code>  if the tetromino is moved successfully
	 */
	public boolean move(Direzione d) {
		if(location == null) {
			System.err.println("Tentativo di spostamento nella griglia di un pezzo non posizionato");
			return false;
		}
		if(isMovable(d)) {
			translate(d);
			return true;
		}
		return false;
	}
	
	/**Moves the tetromino in the supplied direction by a supplied number of blocks,
	 * checking for collision with the game grid boundaries or other monominos in the grid.
	 * @param d The direction where the tetromino will be translated to.
	 * @param blocks The quantity of blocks the tetromino has to be moved.
	 * @return 
	 * 		<code>false</code> if the tetromino couldn't be moved in the supplied direction
	 * 		<br/>
	 * 		<code>true</code>  if the tetromino is moved successfully
	 */
	public boolean move(Direzione d, int blocks) {
		if(location == null) {
			System.err.println("Tentativo di spostamento nella griglia di un pezzo non posizionato");
			return false;
		}
		if(puoMuovere(d, blocks)) {
			translate(d, blocks);
			return true;
		}
		return false;
	}

	/**Moves the tetromino one block in the supplied direction,
	 * without checking for collisions
	 * @param d The direction where the tetromino will be translated to.
	 * */
	private void translate(Direzione d) {
		switch(d) {
		case destra:
			location.translate(Monomino.SIZE, 0);
			break;
		case sinistra:
			location.translate(-Monomino.SIZE, 0);
			break;
		case giu:
			location.translate(0, Monomino.SIZE);
			break;
		case su:
			location.translate(0, -Monomino.SIZE);
			break;
		}
		for (Monomino m : monominos) {
			m.muovi(d);
		}
	}

	/**Moves the tetromino <code>blocks</code> blocks in the supplied direction,
	 * without checking for collisions.
	 * @param d The direction where the tetromino will be translated to.
	 * @param blocks The quantity of blocks the tetromino has to be moved.
	 * */
	private void translate(Direzione d, int blocks) {
		switch(d) {
		case destra:
			location.translate(Monomino.SIZE*blocks, 0);
			break;
		case sinistra:
			location.translate(-Monomino.SIZE*blocks, 0);
			break;
		case giu:
			location.translate(0, Monomino.SIZE*blocks);
			break;
		case su:
			location.translate(0, -Monomino.SIZE*blocks);
			break;
		}
		for (Monomino m : monominos) {
			m.muovi(d, blocks);
		}
	}
	
	/**Returns the grid row index where the tetromino is located*/
	public int getRow() {
		return getLocation().y/Monomino.SIZE;
	}

	/**Returns the grid column index where the tetromino is located*/
	public int getColumn() {
		return getLocation().x/Monomino.SIZE;
	}

	public void mostra() {
		for (Monomino m : monominos)
			grigliaGioco.add(m);
		grigliaGioco.repaint();
	}
	
	/**Returns whether the tetromino can be moved in the supplied direction.
	 * @param d the direction the tetromino should be moved to
	 * @return
	 * 		<code>true</code>  if the tetromino can be moved
	 * 		<code>false</code> if the tetromino would overlap other monominos or if it would be out of the grid boundaries.*/
	public boolean isMovable(Direzione d) {
		translate(d);
		for (Monomino main : monominos) {
			if(grigliaGioco.inBounds(main) == 2){
				translate(Monomino.reverse(d));
				return false;
			}
		}
		translate(Monomino.reverse(d));
		return true;
	}
	
	/**Returns whether the tetromino can be moved in the supplied direction.
	 * @param d the direction the tetromino should be moved to
	 * @param blocks the number of blocks the tetromino should be moved
	 * @return
	 * 		<code>true</code>  if the tetromino can be moved
	 * 		<code>false</code> if the tetromino would overlap other monominos or if it would be out of the grid boundaries.*/
	public boolean puoMuovere(Direzione d, int blocks) {
		translate(d, blocks);
		for (Monomino main : monominos) {
			if(grigliaGioco.inBounds(main) == 2){
				translate(Monomino.reverse(d), blocks);
				return false;
			}
		}
		translate(Monomino.reverse(d), blocks);
		return true;
	}
	
	/**Rotates the tetromino clockwise, checking for collisions with grid boundaries
	 * and other monominos in the grid. If the rotated tetromino wouldn't pass the
	 * collision test, it will be wiggled in every possible direction by one block,
	 * then two blocks until one of the generated locations is valid. If no location 
	 * results valid, the tetromino won't be rotated.
	 * @return
	 * 		<code>true</code>  If the rotation was successful.
	 * 		<code>false</code> If the rotation would lead to an incoherent placement.*/
	public boolean rotateClockwise() {
		rotate();
		if(isLocationValid())
			return true;
		
		for(Direzione d : Direzione.values())
			if(d != Direzione.giu){
				if(move(d))
					return true;
				if(move(d, 2))
					return true;	
			}	
		rotateCounterClockwise();
		return false;
	}
	
	/**Rotates counter-clockwise the tetromino by 90 degrees.*/
	protected void rotateCounterClockwise() {
		rotate();
		rotate();
		rotate();
	}
	
	/**Abstract tetromino rotation method
	 * <br/>
	 * Rotates the tetromino clockwise by 90 degrees, without checking for collisions.
	 * <br/>
	 * This should be implemented in the subclasses based on the different monomino configurations.
	 * */
	protected abstract void rotate();
		
	/***/
	public boolean isLocationValid() {
		boolean posInvalida = false;
		for(Monomino m : monominos)
			posInvalida |= (grigliaGioco.inBounds(m) == 2);
		return !posInvalida;
	}
	
	/**Initialization method to set up the monominos forming the tetromino.
	 * <br/>
	 * This should be implemented in the subclasses based on the different monomino configurations.*/
	protected abstract void generateMonominos();
	
	/**Returns the monominos forming the tetromino*/
	public Monomino[] getMonominos() {
		return this.monominos;
	}
	
	/**Ritorna la posizione del pezzo nella griglia*/
	public Point getLocation() {
		return this.location;
	}
	
	/**Updates the rotation status of the tetromino. This does not actually 
	 * rotate the tetromino! To accomplish that, call {@link rotate()} or {@link rotateClockwise()}*/
	protected void setRotation(RotationStatus sr) {
		this.rotStatus = sr;
	}
	
	/**Returns the rotation status of the tetromino*/
	public RotationStatus setRotation() {
		return this.rotStatus;
	}

}