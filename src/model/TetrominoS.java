package model;

import java.awt.Color;
import java.awt.Point;

import view.Griglia;
import view.Monomino;
import view.Monomino.Direzione;

public class TetrominoS extends Tetromino {

	public TetrominoS(Point location, Griglia gameGrid) {
		super(location, gameGrid);
	}

	/**Ruota il pezzo in senso orario secondo le modalit' indicate su: www/....*/
	public void rotate() {
		switch (setRotation()) {
		case a:
			monominos[0].muovi(Direzione.destra, 2);
			monominos[3].muovi(Direzione.giu, 2);
			setRotation(RotationStatus.b);
			break;
		case b:
			monominos[3].muovi(Direzione.sinistra);
			monominos[2].muovi(Direzione.sinistra);
			monominos[2].muovi(Direzione.giu, 2);
			setRotation(RotationStatus.c);
			break;
		case c:
			monominos[0].muovi(Direzione.sinistra, 2);
			monominos[2].muovi(Direzione.su, 2);
			setRotation(RotationStatus.d);
			break;
		case d:
			monominos[2].muovi(Direzione.destra);
			monominos[3].muovi(Direzione.destra);
			monominos[3].muovi(Direzione.su, 2);
			setRotation(RotationStatus.a);
			break;
		default:
			break;
		}
	}

	/**Generates the four monominos in an S shaped figure.*/
	@Override
	protected void generateMonominos() {
		Point loc = getLocation();
		monominos[3] = new Monomino(loc.x, loc.y-(Monomino.SIZE*2), Color.GREEN);
		monominos[2] = new Monomino(loc.x-Monomino.SIZE, loc.y-(Monomino.SIZE*2), Color.GREEN);
		monominos[1] = new Monomino(loc.x-Monomino.SIZE, loc.y-Monomino.SIZE, Color.GREEN);
		monominos[0] = new Monomino(loc.x-(Monomino.SIZE*2), loc.y-Monomino.SIZE, Color.GREEN);
		
	}

}
