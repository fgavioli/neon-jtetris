package model;

import java.awt.Point;
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.KeyboardHandler;
import view.Griglia;


/**Classe principale del model contenente i dati 
 * di gioco principali sulla partita in corso*/
public class BaseGame implements Griglia.OnGameEndListener {

	/**Coda contenente i prossimi pezzi in discesa sulla griglia*/
	private ArrayBlockingQueue<Tetromino> prossimiPezzi;
	
	/**Griglia grafica contenente i monomini in gioco*/
	private Griglia grigliaGioco;
	
	/**Pezzo in discesa nella griglia*/
	private Tetromino inDiscesa;
	
	/**Timer per discesa del pezzo attivo*/
	private Timer timerDiscesa;
	
	/**Tempo in millisecondi tra due discese di un quadretto del tetromino inDiscesa (velocità di gioco)*/
	private long ritAttuale;

	/**Boolean value indicating if the game is running or stopped*/
	private boolean stop;
	
	/**Costruttore
	 * @param pannelloSfondo Finestra dove disegnare il gioco
	 * @param gridX		Posizione nell'asse x della griglia di gioco
	 * @param gridY		Posizione nell'asse y della griglia di gioco
	 * @param altezza	L'altezza della griglia di gioco (misurata in monomini)
	 * @param larghezza	La larghezza della griglia di gioco (misurata in monomini)
	 * @param dimCoda	Dimensione della coda che conterrà i prossimi pezzi di gioco
	 * */
	public BaseGame(JPanel pannelloSfondo, int gridX, int gridY,  int altezza, int larghezza, int dimCoda) {
		grigliaGioco = new Griglia(gridX, gridY, altezza, larghezza, this);
		prossimiPezzi = new ArrayBlockingQueue<>(dimCoda);
		for(int i = 0; i < dimCoda; i++)
			prossimiPezzi.add(generateNewTetromino(new Point(125, 25)));
		timerDiscesa = new Timer("Discesa");
		KeyboardHandler gt = new KeyboardHandler(this);
		SwingUtilities.getWindowAncestor(pannelloSfondo).addKeyListener(gt);
		pannelloSfondo.add(grigliaGioco);
		stop = true;
	}

	/**Starts the game.*/
	public void startGame() {
		setNewDescending();
		ritAttuale = 500L;
		timerDiscesa.scheduleAtFixedRate(new DescentTask(this), 2000L, ritAttuale);
		stop = false;
	}
	
	/**Ritorna una nuova istanza di una sottoclasse di {@link Tetromino} scelta pseudocasualmente*/
	public Tetromino generateNewTetromino(Point genPoint) {
		Random r = new Random();
		int n = r.nextInt(7);
		Tetromino ret;
		//TEST CODE (DA TENERE COME POSIZIONE BASE DEL TETROMINO ALLO SPAWN)
		
		switch(n) {
		case 0:
			ret = new TetrominoI(genPoint, grigliaGioco);
			break;
		case 1:
			ret = new TetrominoO(genPoint, grigliaGioco);
			break;
		case 2:
			ret = new TetrominoJ(genPoint, grigliaGioco);
			break;
		case 3:
			ret = new TetrominoL(genPoint, grigliaGioco);
			break;
		case 4:
			ret = new TetrominoS(genPoint, grigliaGioco);
			break;
		case 5:
			ret = new TetrominoZ(genPoint, grigliaGioco);
			break;
		case 6:
			ret = new TetrominoT(genPoint, grigliaGioco);
			break;
		default:
			ret = null;
		}
		//return new TetrominoL(genPoint, grigliaGioco);
		return ret;
	}

	/**Pulls the last tetromino element from the next tetrominos queue, 
	 * then pushes a new tetromino element in the queue using {@link #generateRandomTetromino()}.
	 * @return an istance of {@link #Tetromino}, if the queue isn't empty.
	 * <br/>
	 * {@code null} if the queue is empty*/
	public Tetromino getProssimoPezzo() {
		Tetromino ret = prossimiPezzi.poll();
		prossimiPezzi.add(generateNewTetromino(new Point(125, 25)));
		return ret;
	}
	
	/**Adds the current descending tetromino to the grid and prepares a new descending one.*/
	public void setNewDescending(){
		if(inDiscesa!= null)
			grigliaGioco.addTetromino(inDiscesa);
		inDiscesa = getProssimoPezzo();
		inDiscesa.mostra();
	}

	/**Returns the game grid.*/
	public Griglia getGrigliaGioco() {
		return grigliaGioco;
	}

	/**Returns the descending tetromino*/
	public Tetromino getPezzoInDiscesa() {
		return inDiscesa;
	}
	
	public long getRitardoAttuale() {
		return ritAttuale;
	}
	
	public boolean isStopped() {
		return stop;
	}
	
	public void restartTimerDiscesa() {
		timerDiscesa.cancel();
		timerDiscesa = new Timer("Descent");
		timerDiscesa.schedule(new DescentTask(this), getRitardoAttuale(), getRitardoAttuale());
	}

	@Override
	public void onGameEnd() {
		stop = true;
		timerDiscesa.cancel();
		JOptionPane.showMessageDialog(null, "Partita conclusa!");
	}
}