package model;

import java.util.TimerTask;

import view.Monomino.Direzione;

/**Timer task to make the descending tetromino 
 * fall down one block for each timer tick.*/
public class DescentTask extends TimerTask{

	/**Game instance.*/
	private BaseGame game;
	
	/**Constructor
	 * @param g game instance*/
	public DescentTask(BaseGame g) {
		this.game = g;
	}

	/**Every timer tick the descending monomino will be 
	 * moved down by one block. If the movement is not 
	 * possible, a new monomino will be generated for
	 * descent.
	 * */
	@Override
	public void run() {
		if(!game.isStopped()) {
			Tetromino inDescent = game.getPezzoInDiscesa();
			if(!inDescent.move(Direzione.giu))
				game.setNewDescending();
			game.getGrigliaGioco().revalidate();
		}	
	}
}
