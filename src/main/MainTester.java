package main;

import java.awt.FlowLayout;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import model.BaseGame;
import view.PannelloSfondo;

public class MainTester {
	
	public static void main(String[] args) {
		JFrame f = new JFrame("JTetris");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new FlowLayout());
		PannelloSfondo pannelloSfondo = new PannelloSfondo(new ImageIcon("img/mainscreen.png"), 0, 0, 1000, 720);
		f.setContentPane(pannelloSfondo);
		f.setBounds(new Rectangle(0, 0, 1000, 720));
		f.setVisible(true);
		BaseGame g = new BaseGame(pannelloSfondo, 376, 145, 20, 10, 3);
		pannelloSfondo.revalidate();
		pannelloSfondo.repaint();
		g.startGame();
	}
}
