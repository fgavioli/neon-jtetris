package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import model.BaseGame;
import model.Tetromino;
import view.Monomino.Direzione;

public class KeyboardHandler extends KeyAdapter {
	
	private BaseGame game;
	
	/**Constructor for the KeyboardHandler class.
	 * @param g, the game object to act on when a key is pressed
	 * */
	public KeyboardHandler(BaseGame g) {
		super();
		game = g;
	}
    

	/**Key pressed event callback.
	 * Acts on the game object to move the descending Tetromino according to the pressed key*/
	@Override
    public void keyPressed(KeyEvent event) {
		if(!game.isStopped()) {
			Tetromino descending = game.getPezzoInDiscesa();
			switch(event.getKeyCode()) {
	    	case KeyEvent.VK_LEFT:
	    //		if(inDiscesa.puoMuovere(Direzione.giu) && inDiscesa.puoMuovere(Direzione.sinistra))
	    //			gioco.restartTimerDiscesa();
	    		descending.move(Direzione.sinistra);
	    		break;
	    	case KeyEvent.VK_RIGHT:
	    //		if(inDiscesa.puoMuovere(Direzione.giu) && inDiscesa.puoMuovere(Direzione.destra))
	    //			gioco.restartTimerDiscesa();
	    		descending.move(Direzione.destra);
	    		break;
	    	case KeyEvent.VK_UP:
	    		descending.rotateClockwise();
	    		break;
	    	case KeyEvent.VK_DOWN:
	    		if(!descending.move(Direzione.giu))
	    			game.setNewDescending();
	    		break;
			}
		}
	}
}